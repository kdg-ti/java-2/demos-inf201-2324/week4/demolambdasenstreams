package be.kdg.java2;

import java.util.ArrayList;
import java.util.List;
import java.util.function.Predicate;

public class StudentlijstVerwerker {
    /*public List<Student> filterStudenten(List<Student> startlist, FilterFunctie<Student> f){
        List<Student> gefilterdeLijst = new ArrayList<>();
        for (Student s: startlist) {
            if (f.filter(s)) {
                gefilterdeLijst.add(s);
            }
        }
        return gefilterdeLijst;
    }*/

    public List<Student> filterStudenten(List<Student> startlist, Predicate<Student> f){
        List<Student> gefilterdeLijst = new ArrayList<>();
        for (Student s: startlist) {
            if (f.test(s)) {
                gefilterdeLijst.add(s);
            }
        }
        return gefilterdeLijst;
    }
}
