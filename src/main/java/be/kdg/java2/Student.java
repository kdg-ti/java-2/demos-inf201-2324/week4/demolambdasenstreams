package be.kdg.java2;

import java.time.LocalDate;

public class Student {
    private String name;
    private double length;

    public Student(String name, double length) {
        this.name = name;
        this.length = length;
    }

    public String getName() {
        return name;
    }

    public double getLength() {
        return length;
    }

    @Override
    public String toString() {
        return "Student{" +
                "name='" + name + '\'' +
                ", length=" + length +
                '}';
    }
}
