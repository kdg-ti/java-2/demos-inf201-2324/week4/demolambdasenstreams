package be.kdg.java2;

import java.time.LocalDate;
import java.util.*;
import java.util.stream.Stream;

public class Main {
    public static void main(String[] args) {
       /* MetaKlasse m = new MetaKlasse();
        m.voer10KeerUit(new Functie());
        m.voer10KeerUit(new Functie2());
        SuperFunctie sf = new SuperFunctie() {
            @Override
            public void deFunctie() {
                System.out.println("Dag allemaal!");
            }
        };
        SuperFunctie sf2 = () -> System.out.println("Dag allemaal!");
        m.voer10KeerUit(sf);*/
        List<Student> studentList = new ArrayList<>();
        studentList.add(new Student("Jos", 1.78));
        studentList.add(new Student("An", 1.68));
        studentList.add(new Student("Mieke", 1.88));
        studentList.add(new Student("Frits", 2));
       /* StudentlijstVerwerker sv = new StudentlijstVerwerker();
        List<Student> gefilterde = sv.filterStudenten(studentList, s -> {
            System.out.println("Hallo vanuit de lambda!");
            return s.getLength() > 1.8;
        });
        for (Student student : gefilterde) {
            System.out.println(student);
        }*/
        System.out.println("DEMO STREAMS");
        //zet lijst om naar stream en print af
        studentList.stream().forEach(System.out::println);
        //zet lijst om naar stream en filter op lengte
        System.out.println("Filter op lengte:");
        studentList.stream()
                .filter(s -> s.getLength() > 1.8)
                .forEach(System.out::println);
        //klassieke manier:
        for (Student student : studentList) {
            if (student.getLength() > 1.8) {
                System.out.println(student);
            }
        }

        //genereer lijst van willekeurige studenten
        Random r = new Random();
        List<Student> randomStudents = Stream.generate(() -> new Student("jos" + r.nextInt(200), 1 + r.nextDouble()))
                .limit(100).toList();
        //map van student naar string
        randomStudents.stream()
                .map(student -> student.getName())
                .filter(naam -> naam.length() > 5)
                .forEach(System.out::println);
        //map van student naar double
        double totaleLengte = randomStudents.stream().mapToDouble(Student::getLength).sum();
        System.out.println("Totale lengte:" + totaleLengte);
        //geef de langste student
        Optional<Student> optionalStudent = randomStudents.stream().max(Comparator.comparingDouble(Student::getLength));
        if (optionalStudent.isPresent()) {
            System.out.println(optionalStudent.get());
        }
    }

}
