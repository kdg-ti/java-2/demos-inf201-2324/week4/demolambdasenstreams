package be.kdg.java2;

@FunctionalInterface
public interface FilterFunctie<T> {
    boolean filter(T s);
}
